<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.1.0
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/public
 * @author     Anton Syuvaev <anton@elustro.ru>
 */
class Godaddy_Reseller_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Wordpress options section.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      bool|mixed $settings Array of WordPress options section or false.
	 */
	private $settings;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param      string $plugin_name The name of the plugin.
	 * @param      string $version The version of this plugin.
	 * @param      mixed $settings Array of plugin settings.
	 */
	public function __construct( $plugin_name, $version, $settings ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->settings    = $settings;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Godaddy_Reseller_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Godaddy_Reseller_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$plugin_css = plugin_dir_url( __FILE__ ) . 'css/godaddy-reseller-public.css';

		if (!isset($this->settings['styles'])) {
			wp_enqueue_style( $this->plugin_name, $plugin_css, array(), $this->version, 'all' );
		}

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Godaddy_Reseller_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Godaddy_Reseller_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/godaddy-reseller-public.js', array( 'jquery' ), $this->version, false );

	}


	public function get_menu_code($echo = false) {
		$result = false;
		if ( ! empty( $this->settings['id'] ) ) {
			$menu   = new Godaddy_Reseller_Menu( $this->plugin_name, $this->settings );
			$result = $menu->get_menu_html();
		}

		if ( $echo ) {
			echo $result;
		} else {
			return $result;
		}
	}

	public function get_domains_search_form($echo = false) {
		$result = false;
		if ( ! empty( $this->settings['id'] ) ) {
			$menu   = new Godaddy_Reseller_Search_Form( $this->plugin_name, $this->settings );
			$result = $menu->get_form();
		}

		if ( $echo ) {
			echo $result;
		} else {
			return $result;
		}
	}

	public function register_shortcodes() {
		add_shortcode( 'gdrs-menu', array( $this, 'get_menu_code' ) );
		add_shortcode( 'gdrs-form', array( $this, 'get_domains_search_form' ) );
	}

}
