<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      0.1.0
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/admin/partials
 */
?>
<div class="wrap">
	<div class="gdrs-wrapper">
		<header class="gdrs-header">
			<div class="gdrs-header--godaddy">
				<?php _e( 'Customization for your', $this->plugin_name ); ?>
				<img class="gdrs-header--godaddy-logo" src="<?= $this->plugin_admin_url( 'images/GoDaddy.png' ); ?>"
				     alt="GoDaddy"/>
				<?php _e( 'reseller account', $this->plugin_name ); ?>
			</div>
			<img class="gdrs-header--logo" src="<?= $this->plugin_admin_url( 'images/snap-radish-logo.png' ); ?>"
			     alt="Snap radish"/>

			<h2 class="gdrs-header--title"><?php _e( 'GoDaddy Reseller plugin', $this->plugin_name ); ?></h2>
		</header>
		<div class="gdrs-content">

			<?php if ( ! empty( $_GET['settings-updated'] ) && $_GET['settings-updated'] === 'true' ) { ?>
				<div class="updated inline">
					<p><strong><?php _e( 'Settings saved.', $this->plugin_name ); ?></strong></p>
				</div>
			<?php } ?>

			<h2 class="gdrs-header--title"><?php _e( "Let's get started!", $this->plugin_name ); ?></h2>

			<form action="<?= esc_url( admin_url( 'options.php' ) ); ?>" method="post" id="gdrslr-config"
			      accept-charset="<?= esc_attr( get_bloginfo( 'charset' ) ); ?> ">
				<?php settings_fields( $this->plugin_name );
				do_settings_sections( $this->plugin_name );
				?>
				<table class="form-table">
					<tbody>
					<tr>
						<th scope="row"><label
								for="godaddy_reseller_id"><?php _e( 'GoDaddy reseller ID', $this->plugin_name ); ?></label>
						</th>
						<td><input name="<?= $this->plugin_name; ?>[id]" type="text" id="godaddy_reseller_id"
						           value="<?php echo esc_attr( $this->settings['id'] ?: '' ); ?>" class="regular-text">
						</td>
					</tr>
					</tbody>
				</table>

				<div class="gdrs-tabs">
					<ul class="gdrs-tabs--header">
						<li class="gdrs-tabs--tab active"
						    data-target="#gr_tab_styling"><?php _e( 'Styling', $this->plugin_name ); ?></li>
						<li class="gdrs-tabs--tab" data-target="#gr_tab_menu"><?php _e( 'Menu options', $this->plugin_name ); ?></li>
						<li class="gdrs-tabs--tab" data-target="#gr_tab_usage"><?php _e( 'Usage instructions', $this->plugin_name ); ?></li>
					</ul>
					<div class="gdrs-tabs--content">
						<div class="gdrs-tabs--tab-content active" id="gr_tab_styling">
							<table class="form-table">
								<tbody>
								<tr>
									<th scope="row"><label
											for="godaddy_reseller_color_theme"><?php _e( 'Color theme for elements', $this->plugin_name ); ?></label>
									</th>
									<td>
										<select name="<?= $this->plugin_name; ?>[color_theme]"
										        id="godaddy_reseller_color_theme">
											<option
												value="1" <?php selected( $this->settings['color_theme'], 1 ); ?>><?php _e( 'Black', $this->plugin_name ); ?></option>
											<option
												value="2" <?php selected( $this->settings['color_theme'], 2 ); ?>><?php _e( 'White', $this->plugin_name ); ?></option>
											<option
												value="3" <?php selected( $this->settings['color_theme'], 3 ); ?>><?php _e( 'Red', $this->plugin_name ); ?></option>
										</select>

										<div class="gdrs-theme-preview">
											<script>
												var gdrs_theme_preview_path = '<?= $this->plugin_admin_url( 'images/themes' ); ?>';
											</script>
											<img
												src="<?= $this->plugin_admin_url( 'images/themes' ); ?>/<?= $this->settings['color_theme']; ?>.png"
												alt="<?php _e( 'Theme preview', $this->plugin_name ); ?>"
												class="gdrs-theme-preview--img" id="gdrs-theme-preview--img"/>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row"><?php _e( 'Disable styling' ); ?></th>
									<td>
										<fieldset>
											<legend class="screen-reader-text">
												<span><?php _e( 'Disable styling' ); ?></span>
											</legend>
											<label for="godaddy_disable_styles">
												<input name="<?= $this->plugin_name; ?>[styles]" type="checkbox"
												       id="godaddy_disable_styles"
												       value="1"<?php checked( isset( $this->settings['styles'] ) ); ?>>
												<small><?php _e( 'Check this if you don\'t want to use the default styles and will provide the CSS in your own file.' ); ?></small>
											</label>
										</fieldset>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						<div class="gdrs-tabs--tab-content" id="gr_tab_menu">
							<p><?php _e( 'Get premium version to get the ability to choose which elements you want to display!', $this->plugin_name ); ?></p>
						</div>
						<div class="gdrs-tabs--tab-content" id="gr_tab_usage">
							<h3><?php _e( 'Inserting the search form and menu', $this->plugin_name ); ?></h3>
							<h4><?php _e( 'Using shortcodes', $this->plugin_name ); ?></h4>

							<p><?php _e( 'Plugin provides two shortcodes for displaying the domains search form and the resellers menu. Just insert <code>[gdrs-form]</code> or <code>[gdrs-menu]</code> inside your post/page content area.', $this->plugin_name ); ?></p>
							<h4><?php _e( 'Using functions', $this->plugin_name ); ?></h4>

							<p><?php _e( 'If you want to use plugin functionality inside of your template you can use these two functions: <code>godaddy_reseller_form()</code> and <code>godaddy_reseller_menu()</code>. Both function output (echo) the code. If you want to save the code into variable pass <code>false</code> as parameter. E.g. <code>$menu_code = godaddy_reseller_menu(false)</code>.', $this->plugin_name ); ?></p>

							<h3><?php _e( 'Styling', $this->plugin_name ); ?></h3>

							<p><?php _e( 'The plugin has some styling provided for the search form and the menu. You can override that default styling by ticking the "Disable styling" checkbox and providing your CSS-rules in your main css file.', $this->plugin_name ); ?></p>
						</div>
					</div>
				</div>

				<?php submit_button( __( 'Save settings', $this->plugin_name ) ); ?>
			</form>
			<div class="gdrs-getpro">
				<img src="<?= $this->plugin_admin_url( 'images/get-pro.png' ); ?>" alt="Gett Pro version today!"/>
			</div>
		</div>
	</div>
</div>