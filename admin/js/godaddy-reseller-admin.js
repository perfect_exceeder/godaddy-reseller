(function ($) {
	'use strict';

	/**
	 * All of the code for your Dashboard-specific JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */

	$(function () {
		var $body = $('body'), $window = $(window);

		var $theme_preview = $('#gdrs-theme-preview--img');

		$body
			.on('click.gdrs', '.collapse-box--header', function (e) {
				e.preventDefault();
				$(this).parent('.collapse-box').toggleClass('active');

			})
			.on('change.gdrs', '#godaddy_reseller_color_theme', function (e) {
				$theme_preview.attr('src', gdrs_theme_preview_path + '/' + $(this).val() + '.png');
			})
			.on('click.gdrs', '.gdrs-tabs--tab', function(e){
				var _self = $(this), _target = $(_self.data('target'));
				_self.siblings().removeClass('active').end().addClass('active');
				_target.siblings().removeClass('active').end().addClass('active');
			});

	});


})(jQuery);
