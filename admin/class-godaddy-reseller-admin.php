<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.1.0
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/admin
 * @author     Anton Syuvaev <anton@elustro.ru>
 */
class Godaddy_Reseller_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Wordpress options section.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      bool|mixed $settings Array of WordPress options section or false.
	 */
	private $settings;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param      string $plugin_name The name of this plugin.
	 * @param      string $version The version of this plugin.
	 * @param      mixed $settings Array of plugin settings.
	 */
	public function __construct( $plugin_name, $version, $settings ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->settings    = $settings;

	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Godaddy_Reseller_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Godaddy_Reseller_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/godaddy-reseller-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Godaddy_Reseller_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Godaddy_Reseller_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/godaddy-reseller-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add subpage in dashboard
	 *
	 * @since     0.1.0
	 */
	public function add_admin_section() {

		$menu_page = add_menu_page(
			__( 'GoDaddy Reseller', $this->plugin_name ),
			__( 'GoDaddy Reseller', $this->plugin_name ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'show_admin_section' ),
			plugin_dir_url( __FILE__ ) . 'images/snap-radish-icon.png',
			55.30 );
	}

	/**
	 * Function for displaying the admin section
	 *
	 * @since 0.1.0
	 */
	public function show_admin_section() {
		require_once( $this->plugin_admin_path( 'partials/godaddy-reseller-admin-display.php' ) );
	}

	/**
	 * Function for registering WordPress options section
	 *
	 */
	public function register_settings() {
		register_setting( $this->plugin_name, $this->plugin_name );
	}

	public function plugin_admin_url( $file = '' ) {
		return plugin_dir_url( __FILE__ ) . $file;
	}

	public function plugin_admin_path( $file = '' ) {
		return plugin_dir_path( __FILE__ ) . $file;
	}

}
