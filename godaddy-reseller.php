<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             0.1.0
 * @package           Godaddy_Reseller
 *
 * @wordpress-plugin
 * Plugin Name:       GoDaddy Reseller
 * Plugin URI:        http://example.com/godaddy-reseller-uri/
 * Description:       Plugin for GoDaddy Resellers. Provides shortcodes for domain search form and reseller's menu
 * Version:           0.1.0
 * Author:            Snap Radish
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       godaddy-reseller
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-godaddy-reseller-activator.php
 */
function activate_Godaddy_Reseller() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-godaddy-reseller-activator.php';
	Godaddy_Reseller_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-godaddy-reseller-deactivator.php
 */
function deactivate_Godaddy_Reseller() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-godaddy-reseller-deactivator.php';
	Godaddy_Reseller_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_Godaddy_Reseller' );
register_deactivation_hook( __FILE__, 'deactivate_Godaddy_Reseller' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-godaddy-reseller.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_Godaddy_Reseller() {
	$plugin = new Godaddy_Reseller();
	$plugin->run();

}

run_Godaddy_Reseller();

/**
 * Public function for displaying the domains search form
 *
 * @since 0.1.0
 * @param $echo bool
 *
 * @return bool|string Returns true if code was echo-ed or string containing the HTML-code
 */
function godaddy_reseller_form( $echo = true ) {
	$plugin = new Godaddy_Reseller();
	return $plugin->getPluginPublic()->get_domains_search_form( $echo );
}

/**
 * Public function for displaying the big ugly GoDaddy menu
 *
 * @since 0.1.0
 * @param $echo bool
 *
 * @return bool|string Returns true if code was echo-ed or string containing the HTML-code
 */
function godaddy_reseller_menu( $echo = true ) {
	$plugin = new Godaddy_Reseller();
	return $plugin->getPluginPublic()->get_menu_code( $echo );
}