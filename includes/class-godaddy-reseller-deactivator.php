<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      0.1.0
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1.0
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 * @author     Anton Syuvaev <anton@elustro.ru>
 */
class Godaddy_Reseller_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function deactivate() {

	}

}
