<?php
/**
 * Class for displaying the big and ugly reseller's menu.
 *
 * @link       http://example.com
 * @since      0.1.0
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 */

/**
 * Class for displaying the big and ugly reseller's menu.
 *
 * Generate the menu structure and output it in HTML code.
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 * @author     Anton Syuvaev <anton@elustro.ru>
 */
class Godaddy_Reseller_Menu {

	protected $plugin_name;
	protected $settings;

	/**
	 * @var array Array of all menu items and it's children
	 */
	protected $menu;

	/**
	 * Fill the variables
	 *
	 * @param $plugin_name
	 * @param $settings
	 */
	public function __construct( $plugin_name, $settings ) {

		$this->plugin_name = $plugin_name;
		$this->settings    = $settings;
		$this->menu        = $this->create_menu_array();

	}

	/**
	 * Returns the array filled with menu items
	 *
	 * @since 0.1.0
	 * @return array
	 */
	private function create_menu_array() {
		$menu = array();

		$menu[] = array(
			'name'     => __( 'Domain names', $this->plugin_name ),
			'url'      => 'http://www.secureserver.net/domains/search.aspx?ci=1774',
			'children' => array(
				array(
					'name' => __( 'My domain names', $this->plugin_name ),
					'url'  => 'http://dcc.secureserver.net/default.aspx?ci=1812',
				),
				array(
					'name' => __( 'Renew domains', $this->plugin_name ),
					'url'  => 'http://mya.securepaynet.net/MyRenewals/MyRenewals.aspx?ci=8988',
				),
				array(
					'name' => __( 'Assign an AccountExec', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/account/accountexec.aspx?ci=4311',
				),
				array(
					'name' => __( 'Register Domains', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domains/search.aspx?ci=1775',
				),
				array(
					'name' => __( 'Transfer Domains', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domains/transfers.aspx?ci=1776',
				),
				array(
					'name' => __( 'Bulk Registration', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domains/searchbulk.aspx?ci=1777',
				),
				array(
					'name' => __( 'Bulk Transfer', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domains/transfersbulk.aspx?ci=1778',
				),
				array(
					'name' => __( 'Internationalized Domain Names (IDN)', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domains/searchidn.aspx?ci=10993',
				),
				array(
					'name' => __( 'Private Registration', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domainaddon/private-registration.aspx?ci=1779',
				),
				array(
					'name' => __( 'Deluxe Registration', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domainaddon/deluxe-registration.aspx?ci=10661',
				),
				array(
					'name' => __( 'Protected Registration', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domainaddon/protected-registration.aspx?ci=10662',
				),
				array(
					'name' => __( 'Domain Backordering', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domainaddon/domain-alert.aspx?ci=1780',
				),
				array(
					'name' => __( 'Forwarding & Masking', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domainaddon/domain-forwarding.aspx?ci=1781',
				),
				array(
					'name' => __( 'Business Registration', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domainaddon/business-registration.aspx?ci=6304',
				),
				array(
					'name' => __( 'Certified Domain', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/domains/certified-domains.aspx?ci=7020',
				),
			)
		);

		/*
				  <li id="pcm-domains-appraisal" class="pcj_mi"><a href="#" style="font-weight:bold;">Domain Name Appraisals</a></li>

		 * */

		$menu[] = array(
			'name'     => __( 'Web hosting', $this->plugin_name ),
			'url'      => 'http://www.secureserver.net/hosting/web-hosting.aspx?ci=1782',
			'children' => array(
				array(
					'name' => __( 'My Hosting Accounts', $this->plugin_name ),
					'url'  => 'http://mya.securepaynet.net/hosting_ded/dedicatedhosting.aspx?ci=1815',
				),
				array(
					'name' => __( 'Hosting Plans', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/hosting/web-hosting.aspx?ci=1783',
				),
				array(
					'name' => __( 'WordPress® Site/Blog', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/hosting/wordpress.aspx?ci=43232',
				),
				array(
					'name' => __( 'VPS', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/hosting/vps-hosting.aspx?ci=1784',
				),
				array(
					'name' => __( 'Dedicated Servers', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/hosting/dedicated-servers.aspx?ci=1785',
				),
				array(
					'name' => __( 'Dedicated IP', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/ssl/static-ip.aspx?ci=1786',
				),
				array(
					'name' => __( 'Search Engine Visibility', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/search-engine/seo-services.aspx?ci=1787',
				),
			)
		);

		$menu[] = array(
			'name'     => __( 'SSL & Security', $this->plugin_name ),
			'url'      => 'http://www.secureserver.net/gdshop/ssl/ssl.asp?ci=1789',
			'children' => array(
				array(
					'name' => __( 'My SSL Certificates', $this->plugin_name ),
					'url'  => 'http://mya.securepaynet.net/sslcert/ssl.aspx?ci=1819'
				),
				array(
					'name' => __( 'See All Certificates', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/ssl/ssl-certificates.aspx?ci=1790'
				),
				array(
					'name' => __( 'Code Signing Certificates', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/ssl/code-signing-certificate.aspx?ci=13343'
				),
				array(
					'name' => __( 'SiteLock Website Security', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/security/malware-scanner.aspx?ci=89298'
				),
			)
		);

		$menu[] = array(
			'name'     => __( 'Email accounts', $this->plugin_name ),
			'url'      => 'http://www.secureserver.net/email/email-hosting.aspx?ci=1794',
			'children' => array(
				array(
					'name' => __( 'My Email Account', $this->plugin_name ),
					'url'  => 'http://mya.securepaynet.net/products/accountlist.aspx?ci=1817'
				),
				array(
					'name' => __( 'Check My Web Mail', $this->plugin_name ),
					'url'  => 'https://login.secureserver.net/index.php?ci=9094'
				),
				array(
					'name' => __( 'Email Plans', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/email/email-hosting.aspx?ci=12940'
				),
				array(
					'name' => __( 'Hosted Exchange Email', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/email/hosted-exchange.aspx?ci=14629'
				),
				array(
					'name' => __( 'Online Storage', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/email/online-file-storage.aspx?ci=1796'
				),
				array(
					'name' => __( 'Fax Thru Email', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/email/internet-fax.aspx?ci=1797'
				),
				array(
					'name' => __( 'Express Email Marketing®', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/business/email-marketing.aspx?ci=42405'
				),
				array(
					'name' => __( 'Calendar', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/calendar/online-calendar.aspx?ci=1798'
				),
			)
		);

		$menu[] = array(
			'name'     => __( 'Marketing tools', $this->plugin_name ),
			'url'      => 'http://www.secureserver.net/ecommerce/shopping-cart.aspx?ci=1799',
			'children' => array(
				array(
					'name' => __( 'My Ecommerce Products', $this->plugin_name ),
					'url'  => 'http://mya.securepaynet.net/products/accountlist.aspx?ci=14481'
				),
				array(
					'name' => __( 'Quick Shopping Cart®', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/ecommerce/shopping-cart.aspx?ci=1802'
				),
				array(
					'name' => __( 'Search Engine Visibility', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/search-engine/seo-services.aspx?ci=1800'
				),
				array(
					'name' => __( 'Express Email Marketing®', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/business/email-marketing.aspx?ci=42404'
				),
				array(
					'name' => __( 'Fax Thru Email', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/email/internet-fax.aspx?ci=1797'
				),
			)
		);

		$menu[] = array(
			'name'     => __( 'Build a website', $this->plugin_name ),
			'url'      => 'http://www.secureserver.net/hosting/website-builder.aspx?ci=1805',
			'children' => array(
				array(
					'name' => __( 'My Websites', $this->plugin_name ),
					'url'  => 'http://mya.securepaynet.net/sitebuilder/hostingaccountlist.aspx?ci=1814'
				),
				array(
					'name' => __( 'Website Builder', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/hosting/website-builder2.aspx?ci=1806'
				),
				array(
					'name' => __( 'WordPress® Site/Blog', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/hosting/wordpress.aspx?ci=53396'
				),
				array(
					'name' => __( 'Web Design Services', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/design/web-design.aspx?ci=12408'
				),
				array(
					'name' => __( 'Quick Shopping Cart®', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/ecommerce/shopping-cart.aspx?ci=1807'
				),
				array(
					'name' => __( 'eCommerce Web Design', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/design/web-store-design.aspx?ci=52960'
				),
				array(
					'name' => __( 'Search Engine Visibility', $this->plugin_name ),
					'url'  => 'http://www.secureserver.net/search-engine/seo-services.aspx?ci=1808'
				),
			)
		);

		return $menu;
	}

	/**
	 *
	 *
	 * @since 0.1.0
	 * @return array
	 */
	public function get_menu() {
		return $this->menu;
	}

	/**
	 * @return string
	 */
	public function get_menu_html() {
		$result = '';

		$result .= '<nav class="gdrs-nav gdrs-theme-'.$this->settings['color_theme'].'">';


		$result .= $this->print_menu_items( $this->menu );

		$result .= '</nav>';

		return $result;
	}

	/**
	 * @param $items
	 * @param int $depth
	 *
	 * @return string
	 */
	private function print_menu_items( $items, $depth = 0 ) {
		$result  = '';
		$classes = array();

		$classes[] = 'gdrs-nav--list';
		$classes[] = 'gdrs-nav--list__depth-' . $depth;
		if ( $depth ) {
			$classes[] = 'gdrs-nav--list__submenu';
		}

		$result .= '<ul class="' . implode( ' ', $classes ) . '">';
		$i = 1;

		foreach ( $items as $item ) {
			$classes   = array();
			$classes[] = 'gdrs-nav--item';
			$classes[] = 'gdrs-nav--item__depth-' . $depth;
			if ( ! $depth ) {
				$classes[] = 'gdrs-nav--item__columns-' . count( $items );
			} else {
				$classes[] = 'gdrs-nav--item__subitem';
			}
			if ( ! empty( $item['children'] ) ) {
				$classes[] = 'gdrs-nav--item__has-children';
			}
			if ( $i == 1 ) {
				$classes[] = 'gdrs-nav--item__first';
			}

			if ( $i == count( $items ) ) {
				$classes[] = 'gdrs-nav--item__last';
			}


			$result .= '<li class="' . implode( ' ', $classes ) . '">';

			$classes   = array();
			$classes[] = 'gdrs-nav--link';
			$classes[] = 'gdrs-nav--link__depth-' . $depth;
			if ( $depth ) {
				$classes[] = 'gdrs-nav--link__subitem';
			}

			$result .= '<a class="' . implode( ' ', $classes ) . '" href="' . $this->reseller_url( $item['url'] ) . '">';
			$result .= $item['name'];
			$result .= '</a>';
			if ( ! empty( $item['children'] ) ) {
				$result .= $this->print_menu_items( $item['children'], $depth + 1 );
			}
			$result .= '</li>';
			$i ++;
		}
		$result .= '</ul>';

		return $result;
	}

	/**
	 * Function for generating full URL for reseller service page
	 *
	 * @param $url The URL of page without hte Reseller's ID
	 *
	 * @return string URL with resellers ID
	 */
	private function reseller_url( $url ) {
		$url_parts = parse_url( $url );

		if ( empty( $url_parts['query'] ) ) {
			$url_parts['query'] = '';
		} else {
			$url_parts['query'] .= '&';
		}

		$url_parts['query'] .= 'prog_id=' . $this->settings['id'];

		return $this->unparse_url( $url_parts );
	}

	/**
	 * Reverse function for PHP internal parse_url()
	 *
	 * @link http://php.net/manual/en/function.parse-url.php#106731
	 *
	 * @param $parsed_url
	 *
	 * @return string
	 */
	private function unparse_url( $parsed_url ) {
		$scheme   = isset( $parsed_url['scheme'] ) ? $parsed_url['scheme'] . '://' : '';
		$host     = isset( $parsed_url['host'] ) ? $parsed_url['host'] : '';
		$port     = isset( $parsed_url['port'] ) ? ':' . $parsed_url['port'] : '';
		$user     = isset( $parsed_url['user'] ) ? $parsed_url['user'] : '';
		$pass     = isset( $parsed_url['pass'] ) ? ':' . $parsed_url['pass'] : '';
		$pass     = ( $user || $pass ) ? "$pass@" : '';
		$path     = isset( $parsed_url['path'] ) ? $parsed_url['path'] : '';
		$query    = isset( $parsed_url['query'] ) ? '?' . $parsed_url['query'] : '';
		$fragment = isset( $parsed_url['fragment'] ) ? '#' . $parsed_url['fragment'] : '';

		return "$scheme$user$pass$host$port$path$query$fragment";
	}


}