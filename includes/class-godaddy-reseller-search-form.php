<?php
/**
 * Class for displaying the domains search form.
 *
 * @link       http://example.com
 * @since      0.1.0
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 */

/**
 * Class for displaying the domains search form.
 *
 * Generate the domains search form HTML code.
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 * @author     Anton Syuvaev <anton@elustro.ru>
 */
class Godaddy_Reseller_Search_Form {

	/**
	 * @var string Unique ID of the plugin
	 */
	protected $plugin_name;

	/**
	 * @var mixed plugin settings
	 */
	protected $settings;

	/**
	 * @param $plugin_name string Unique ID of the plugin
	 * @param $godaddy_id string Reseller's ID on GoDaddy site
	 */
	public function __construct( $plugin_name, $settings ) {

		$this->plugin_name = $plugin_name;
		$this->settings  = $settings;

	}

	/**
	 * Compiles the HTML-code for search form
	 *
	 * @since 0.1.0
	 * @return string
	 */
	public function get_form() {
		$result = '';
		if ( $this->settings['id'] ) {
			$result .= '<form action="http://www.secureserver.net/domains/search.aspx?checkAvail=1&amp;prog_id=' . $this->settings['id'] . '" method="post" class="gdrs-form gdrs-form__domain gdrs-theme-'.$this->settings['color_theme'].'">';

			$result .= '<input type="text" name="domainToCheck" class="gdrs-form--input" placeholder="' . __( 'Enter your domain', $this->plugin_name ) . '"/>';

			$result .= '<input type="submit" value="' . __( 'Search', $this->plugin_name ) . '" name="submit" class="gdrs-form--submit"/>';

			$result .= '</form>';
		}

		return $result;
	}

}