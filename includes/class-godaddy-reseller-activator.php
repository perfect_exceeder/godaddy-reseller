<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      0.1.0
 *
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    Godaddy_Reseller
 * @subpackage Godaddy_Reseller/includes
 * @author     Anton Syuvaev <anton@elustro.ru>
 */
class Godaddy_Reseller_Activator {

	/**
	 * Adds options section into DB.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {
		add_option('godaddy-reseller',array('id'=>'','color_theme'=>1));
	}

}
